//
//  ContentObject.swift
//  OverHead
//
//  Created by Tomasz Idzi on 07/01/2017.
//  Copyright © 2017 Tomasz Idzi. All rights reserved.
//

import Foundation

struct ContentObject {
    var name: String?
    var description: String?
    var url: String?
    var isExpended:Bool?
    
    init?(from dictionary: Dictionary<String, Any>) {
        
        guard let name = dictionary["name"] as? String,
            let description = dictionary["description"] as? String,
            let url = dictionary["url"] as? String else {
                return nil
        }
        
        self.name = name
        self.description = description
        self.url = url
        self.isExpended = false
    }
}
