//
//  ContentCell.swift
//  OverHead
//
//  Created by Tomasz Idzi on 07/01/2017.
//  Copyright © 2017 Tomasz Idzi. All rights reserved.
//

import UIKit

protocol ContentCellDelegate{
    func expandButtonTapped(in cell: ContentCell)
    func tapped(on image: UIImage)
}

class ContentCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var expandButton: UIButton!
    
    @IBOutlet weak var pictureHeightConstraint: NSLayoutConstraint!
    
    var delegate: ContentCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showPicture(_:)))
        picture.addGestureRecognizer(tapGesture)
    }

    
    func fillCell(with data: ContentObject) {
        title.text = data.name
        
        if data.isExpended! {
            content.text = data.description
            let url = URL(string:data.url!)!
            picture.af_setImage(withURL: url)
            pictureHeightConstraint.constant = 200
            expandButton.setTitle("▼", for: .normal)
        } else {
            content.text = nil
            picture.image = nil
            pictureHeightConstraint.constant = 0
            expandButton.setTitle("▷", for: .normal)
        }
    }
    
    @IBAction func expand(_ sender: Any) {
        self.delegate.expandButtonTapped(in: self)
    }
    
    @IBAction func showPicture(_ sender: Any) {
        self.delegate.tapped(on: picture.image!)
    }
    
}
