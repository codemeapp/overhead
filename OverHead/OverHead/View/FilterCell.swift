//
//  FilterCell.swift
//  OverHead
//
//  Created by Tomasz Idzi on 09/01/2017.
//  Copyright © 2017 Tomasz Idzi. All rights reserved.
//

import UIKit

class FilterCell: UITableViewCell {

    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var checkMark: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
