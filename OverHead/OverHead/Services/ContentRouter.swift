//
//  ContentRouter.swift
//  OverHead
//
//  Created by Tomasz Idzi on 07/01/2017.
//  Copyright © 2017 Tomasz Idzi. All rights reserved.
//

import Foundation
import Alamofire

public enum ContentRouter: URLRequestConvertible {
    static let baseURLPath = "https://static.mobileinteraction.se/Mistander"
    
    case Images
    
    public func asURLRequest() throws -> URLRequest {
        let result: (path: String, method: Alamofire.HTTPMethod, parameters: [String: AnyObject]) = {
            switch self {
            case .Images:
                return ("/images.json", .get, [String: AnyObject]())
            }
        }()
        let url = try ContentRouter.baseURLPath.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
        urlRequest.httpMethod = result.method.rawValue
        urlRequest.timeoutInterval = TimeInterval(10 * 1000)
        
        return try URLEncoding.default.encode(urlRequest, with: result.parameters)
    }
    
}
