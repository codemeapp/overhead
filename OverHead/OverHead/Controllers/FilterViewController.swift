//
//  FilterViewController.swift
//  OverHead
//
//  Created by Tomasz Idzi on 08/01/2017.
//  Copyright © 2017 Tomasz Idzi. All rights reserved.
//

import UIKit

enum Filters : Int {
    case None
    case Asc
    case Desc
    
    static let filterNames = [
        None : "None",
        Asc : "A-Z",
        Desc : "Z-A"]
    
    func filterName() -> String {
        if let filterName = Filters.filterNames[self] {
            return filterName
        } else {
            return "Filter"
        }
    }
    
    static var count: Int { return Filters.filterNames.count }
}

protocol FilterViewControllerDelegate{
    func sortContent(with Filter: Filters)
}

class FilterViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    var delegate: FilterViewControllerDelegate!
    var selectedFilterIndex:Filters = Filters(rawValue: 0)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
    }

}

extension FilterViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Filters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell", for: indexPath) as! FilterCell
        
        if let filterIndex = Filters(rawValue: indexPath.row) {
            cell.name.text = filterIndex.filterName()
            if filterIndex == selectedFilterIndex {
                cell.checkMark.isHidden = false
            }
        }
        
        return cell
    }

    func  tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate.sortContent(with: Filters(rawValue: indexPath.row)!)
        _ = self.navigationController?.popViewController(animated: true)
    }
}
