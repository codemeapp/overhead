//
//  PictureViewController.swift
//  OverHead
//
//  Created by Tomasz Idzi on 08/01/2017.
//  Copyright © 2017 Tomasz Idzi. All rights reserved.
//

import UIKit

class PictureViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var picture: UIImageView!
    
    // MARK: - Properties
    var imagePicture: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picture.image = imagePicture
    }

    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
