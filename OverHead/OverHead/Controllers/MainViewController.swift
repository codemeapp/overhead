//
//  MainViewController.swift
//  OverHead
//
//  Created by Tomasz Idzi on 07/01/2017.
//  Copyright © 2017 Tomasz Idzi. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class MainViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    var objects: [ContentObject]?
    var refreshControl: UIRefreshControl!
    var selectedFilter: Filters?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 85
        updateData(completionHandler: nil)
        
        //Pull to refresh
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    func refresh(sender:AnyObject) {
        updateData() {
            self.refreshControl.endRefreshing()
        }
        
    }
    
    func updateData(completionHandler: (() -> Swift.Void)? = nil) {
        downloadContent(completion: { objects in
            self.objects = objects
            self.tableView.reloadData()
            completionHandler?()
        })
    }
    
    @IBAction func filters(_ sender: Any) {
        let filterViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        filterViewController.delegate = self
        if let filter = selectedFilter {
            filterViewController.selectedFilterIndex = filter
        }
        self.navigationController?.pushViewController(filterViewController, animated: true)
    }
    
}

// MARK: - FilterViewControllerDelegate
extension MainViewController: FilterViewControllerDelegate {
    func sortContent(with Filter: Filters) {
        selectedFilter = Filter
        switch Filter {
        case .Asc:
            objects?.sort() {
                $0.name! < $1.name!
            }
            tableView.reloadData()
        case .Desc:
            objects?.sort() {
                $0.name! > $1.name!
            }
            tableView.reloadData()
        case .None:
            updateData(completionHandler: nil)
        }
    }
}

// MARK: - UITableViewDataSource
extension MainViewController: UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let data = objects else {
            return 0
        }
        
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContentCell",
                                                 for: indexPath) as! ContentCell
        cell.fillCell(with: (objects?[indexPath.row])!)
        cell.delegate = self
        return cell
    }
    
}

// MARK: - ContentCellDelegate
extension MainViewController: ContentCellDelegate {
    
    func expandButtonTapped(in cell: ContentCell) {
        let indexPath = self.tableView.indexPath(for: cell)
        var object = objects?[(indexPath?.row)!]
        object?.isExpended = !(object?.isExpended)!
        objects?[(indexPath?.row)!] = object!
        
        self.tableView.beginUpdates()
        cell.fillCell(with: object!)
        self.tableView.endUpdates()
        self.tableView.scrollToRow(at: indexPath!, at: .top, animated: true)
    }
    
    func tapped(on image: UIImage) {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PictureViewController") as! PictureViewController
        viewController.imagePicture = image
        self.present(viewController, animated: true, completion: nil)
    }
}

// MARK: - WebService connection
extension MainViewController {
   
    func downloadContent(completion: @escaping ([ContentObject]) -> Void) {
        Alamofire.request(ContentRouter.Images)
            .responseJSON {response in
                
                guard response.result.isSuccess else {
                    print("Error while fetching objects: \(response.result.error)")
                    completion([ContentObject]())
                    return
                }
                
                guard let responseJSON = response.result.value as? [String: AnyObject],
                    let results = responseJSON["files"] as? [AnyObject]
                    else {
                        print("Invalid objects information received from service")
                        completion([ContentObject]())
                        return
                }
                
                let objects = results.flatMap({ (dict) -> ContentObject? in
                    return ContentObject(from: dict as! Dictionary<String, Any>)
                })
                
                completion(objects)
        }
    }
}
